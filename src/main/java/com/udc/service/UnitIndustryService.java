package com.udc.service;

import com.udc.model.UnitIndustry;
import org.springframework.stereotype.Component;

/**
 * Created by sdrahnea
 */
@Component
public class UnitIndustryService extends AbstractService<UnitIndustry> {

}
