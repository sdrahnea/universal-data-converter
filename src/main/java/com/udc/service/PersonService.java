package com.udc.service;

import com.udc.model.Person;
import org.springframework.stereotype.Component;

/**
 * Created by sdrahnea
 */
@Component
public class PersonService extends AbstractService<Person> {

}
