package com.udc.service;

import com.udc.model.Country;
import org.springframework.stereotype.Component;

/**
 * Created by sdrahnea
 */
@Component
public class CountryService extends AbstractService<Country> {

}
