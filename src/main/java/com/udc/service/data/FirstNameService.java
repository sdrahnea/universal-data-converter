package com.udc.service.data;

import com.udc.model.data.FirstName;
import com.udc.service.AbstractService;
import org.springframework.stereotype.Component;

/**
 * Created by sdrahnea
 */
@Component
public class FirstNameService extends AbstractService<FirstName> {

}
