package com.udc.service.data;

import com.udc.model.data.LastName;
import com.udc.service.AbstractService;
import org.springframework.stereotype.Component;

/**
 * Created by sdrahnea
 */
@Component
public class LastNameService extends AbstractService<LastName> {

}
