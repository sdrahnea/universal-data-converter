package com.udc.converter;

import com.udc.model.Country;

import javax.faces.convert.FacesConverter;

/**
 * Created by sdrahnea
 */
@FacesConverter("countryConverter")
public class CountryConverter extends AbstractConverter<Country> {
}
