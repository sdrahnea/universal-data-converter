package com.udc.converter;

import com.udc.model.UnitIndustry;

import javax.faces.convert.FacesConverter;

/**
 * Created by sdrahnea
 */
@FacesConverter("unitIndustryConverter")
public class UnitIndustryConverter extends AbstractConverter<UnitIndustry> {
}
