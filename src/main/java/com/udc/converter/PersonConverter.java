package com.udc.converter;

import com.udc.model.Person;

import javax.faces.convert.FacesConverter;

/**
 * Created by sdrahnea
 */
@FacesConverter("personConverter")
public class PersonConverter extends AbstractConverter<Person> {
}
