package com.udc.converter;

import com.udc.model.Unit;

import javax.faces.convert.FacesConverter;

/**
 * Created by sdrahnea
 */
@FacesConverter("unitConverter")
public class UnitConverter extends AbstractConverter<Unit> {
}
