package com.udc.converter;

import com.udc.model.UnitType;

import javax.faces.convert.FacesConverter;

/**
 * Created by sdrahnea
 */
@FacesConverter("unitTypeConverter")
public class UnitTypeConverter extends AbstractConverter<UnitType> {
}
