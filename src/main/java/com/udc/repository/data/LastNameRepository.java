package com.udc.repository.data;

import com.udc.model.data.LastName;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sdrahnea
 */
public interface LastNameRepository extends CrudRepository<LastName, Long> {

}