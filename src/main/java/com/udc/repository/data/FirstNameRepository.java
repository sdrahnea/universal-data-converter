package com.udc.repository.data;

import com.udc.model.data.FirstName;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sdrahnea
 */
public interface FirstNameRepository extends CrudRepository<FirstName, Long> {

}