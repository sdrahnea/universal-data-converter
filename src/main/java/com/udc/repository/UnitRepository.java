package com.udc.repository;

import com.udc.model.Unit;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sdrahnea
 */
public interface UnitRepository extends CrudRepository<Unit, Long> {

}