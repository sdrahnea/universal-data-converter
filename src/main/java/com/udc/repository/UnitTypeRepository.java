package com.udc.repository;

import com.udc.model.UnitType;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sdrahnea
 */
public interface UnitTypeRepository extends CrudRepository<UnitType, Long> {

}