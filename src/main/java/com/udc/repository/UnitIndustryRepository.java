package com.udc.repository;

import com.udc.model.UnitIndustry;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sdrahnea
 */
public interface UnitIndustryRepository extends CrudRepository<UnitIndustry, Long> {

}