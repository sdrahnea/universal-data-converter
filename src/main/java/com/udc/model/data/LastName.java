package com.udc.model.data;

import com.udc.model.CoreEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by sdrahnea
 */
@Entity
@Table(name = "last_name")
public class LastName extends CoreEntity {

}
